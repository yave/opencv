#include <opencv2\opencv.hpp>
#include <opencv2\highgui\highgui.hpp>

using namespace cv;
using namespace std;

void test1();
void test2();

void main()
{
	test1();
	test2();
}

void test1()
{
	Mat img = imread("E:\\Installers\\OpencvFiles\\OpencvTest\\data\\right01.jpg", IMREAD_GRAYSCALE);
	imshow("img", img);
	KeyPoint kp = KeyPoint(148, 77, 50, -1.0F, 0.0F, 0, -1);
	vector<KeyPoint> v = { kp };
	FAST(img, v, 40, true);
	imshow("img", img);
	waitKey();
}

void test2()
{
	Mat src = imread("E:\\Installers\\OpencvFiles\\OpencvTest\\data\\right01.jpg", IMREAD_GRAYSCALE);
	cout << " Image size :" << src.rows << " " << src.cols << "\n";
	vector<KeyPoint> keypointsD;
	Ptr<FastFeatureDetector> detector = FastFeatureDetector::create();
	vector<Mat> descriptor;

	detector->detect(src, keypointsD, Mat());
	drawKeypoints(src, keypointsD, src);
	imshow("keypoints", src);
	waitKey();
}

